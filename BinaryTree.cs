﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;

namespace Generics.BinaryTrees
{
    public class TreeNode<T> where T: IComparable
    {
        public T Value { get; set; }
        public TreeNode<T> Left { get; set; }
        public TreeNode<T> Right { get; set; }

        public TreeNode(T value)
        {
            Value = value;
        }

        public void Add(T value)
        {
            if (value.CompareTo(Value) <= 0)
            {
                if (Left == null)
                {
                    Left = new TreeNode<T>(value);
                }
                else
                {
                    Left.Add(value);
                }
            }

            else
            {
                if (Right == null) Right = new TreeNode<T>(value);
                else Right.Add(value);
            }
        }
    }

    public class BinaryTree<T> : IEnumerable<T>
        where T:IComparable
    {
        public TreeNode<T> Root;
        public T Value => Root.Value;
        public TreeNode<T> Left => Root.Left;
        public TreeNode<T> Right => Root.Right;

        public BinaryTree()
        {
            Root = null;
        }

        public void Add(T value)
        {
            if (Root == null) Root = new TreeNode<T>(value);
            else Root.Add(value);
        }

        //Не работает без явного указания типа в тестах ( BinaryTree<int>.Create(4, 3, 2, 1) приходится писать так)
        public static BinaryTree<T> Create(params T[] args)
        {
            var binaryTree = new BinaryTree<T>();
            foreach (var arg in args)
            {
                binaryTree.Add(arg);
            }
            return binaryTree;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public List<T> LCR(TreeNode<T> node)
        {
            var list = new List<T>();
            return LCR(node, ref list);
        }

        public List<T> LCR(TreeNode<T> node, ref List<T> result)
        {
            if (node != null)
            {
                LCR(node.Left, ref result);
                result.Add(node.Value);
                LCR(node.Right,ref result);
            }
            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var treeArray = LCR(Root);
            foreach (var node in treeArray)
            {
                yield return node;
            }
        }
    }
}
